if Rails.env.development?
  Sidekiq.configure_server do |config|
    config.redis = { url: 'redis://localhost:6379/12' }
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: 'redis://localhost:6379/12' }
  end
else
  Sidekiq.configure_server do |config|
    config.redis = { url: ENV['REDIS_SIDEKIQ_URL'] }
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: ENV['REDIS_SIDEKIQ_URL'] }
  end
end