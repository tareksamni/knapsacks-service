require 'sidekiq/web'

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  mount Sidekiq::Web => '/monitor'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :knapsack, defaults: { format: :json } do
    resources :tasks, only: [:create, :show]
    resources :solutions, only: [:show]
    namespace :admin do
      resources :tasks, only: [:index]
      post :shutdown
    end
  end
end
