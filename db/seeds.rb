# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

items_count = Faker::Number.between(10, 30)
items_count.times.map { Faker::Number.between(1, 200) }
50.times do
  task_data = {
    status: :submitted,
    capacity: Faker::Number.between(0, 500),
    weights: items_count.times.map { Faker::Number.between(1, 200) },
    values: items_count.times.map { Faker::Number.between(1, 200) }
  }
  Task.create!(task_data)
end