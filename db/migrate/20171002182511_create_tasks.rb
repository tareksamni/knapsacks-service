class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :status, null: false
      t.datetime :started_at
      t.datetime :completed_at
      t.jsonb :problem
      t.jsonb :solution

      t.timestamps
    end
  end
end
