class AddProblemFieldsToTasks < ActiveRecord::Migration[5.1]
  def change
    add_column :tasks, :capacity, :integer
    add_column :tasks, :values, :jsonb
    add_column :tasks, :weights, :jsonb
    remove_column :tasks, :problem
  end
end
