class Task < ApplicationRecord
  STATUS_COMPLETED = 'completed'.freeze
  STATUS_STARTED = 'started'.freeze
  STATUS_SUBMITTED = 'submitted'.freeze
  validates :status, presence: true
  validates :status, inclusion: { in: [STATUS_COMPLETED, STATUS_STARTED, STATUS_SUBMITTED],
                                  message: "%{value} is not a valid status" }
  validates :capacity, presence: true
  validates :values, presence: true
  validates :weights, presence: true

  scope :submitted, -> { where(status: STATUS_SUBMITTED) }
  scope :started, -> { where(status: STATUS_STARTED) }
  scope :completed, -> { where(status: STATUS_COMPLETED) }

  state_machine :status, initial: :submitted do
    event :start do
      transition submitted: :started
    end
    event :complete do
      transition started: :completed
    end

    before_transition on: :start do |task, transition|
      task.started_at = Time.now
    end

    before_transition on: :complete do |task, transition|
      task.completed_at = Time.now
    end
  end

  def completed?
    status == STATUS_COMPLETED
  end

  def solution_time
    return 0 if started_at.nil? || completed_at.nil? || !completed?
    (completed_at - started_at).to_i
  end

  before_validation(on: :create) do
    self.solution ||= []
    self.status ||= :submitted
  end
end
