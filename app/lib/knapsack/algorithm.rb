class Knapsack::Algorithm
  def self.solve(items, capacity)
    validate_params!(items, capacity)
    return {
      indices: [],
      value: 0,
      cost: 0
    } if items.empty?
    num_items = items.size
    cost_matrix = Array.new(num_items){Array.new(capacity + 1, 0)}

    num_items.times do |i|
      (capacity + 1).times do |j|
        if(items[i].weight > j)
          cost_matrix[i][j] = cost_matrix[i-1][j]
        else
          cost_matrix[i][j] = [cost_matrix[i-1][j], items[i].value + cost_matrix[i-1][j-items[i].weight]].max
        end
      end
    end
    used_items = get_used_items(items, cost_matrix)
    {
      indices: indices(used_items),
      value: items.zip(used_items).map{|item,used| item.weight*used}.inject(:+),
      cost: cost_matrix.last.last
    }
  end

  def self.get_used_items(items, cost_matrix)
    i = cost_matrix.size - 1
    currentCost = cost_matrix[0].size - 1
    marked = cost_matrix.map { 0 }

    while(i >= 0 && currentCost >= 0)
      if(i == 0 && cost_matrix[i][currentCost] > 0 ) || (cost_matrix[i][currentCost] != cost_matrix[i-1][currentCost])
        marked[i] = 1
        currentCost -= items[i].weight
      end
      i -= 1
    end
    marked
  end

  def self.indices(used_items)
    used_items.each_index.select { |i| used_items[i].positive? }
  end

  def self.validate_params!(items, capacity)
    unless items.is_a?(Array)
      error = "Expected array of KnapsackItems but got: #{items.class}"
      raise ArgumentError, error
    end

    unless items.all? { |i| i.is_a?((Knapsack::Item)) }
      items_to_str = items.map { |i| i.inspect }.join(", ")
      error = "Expected array of KnapsackItems but got: #{items_to_str}"
      raise ArgumentError, error
    end

    unless capacity.is_a?(Integer)
      error = "Expected capacity as Integer but got: #{capacity.class}"
      raise ArgumentError, error
    end
  end
end