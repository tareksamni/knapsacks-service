class Knapsack::TasksController < ApplicationController
  def show
    @task = Task.find(params[:id])
  end

  def create
    @task = Task.new(task_params)
    if @task.save
      SolveWorker.perform_async(@task.id)
      render :show
    else
      handle_errors(@task)
    end
  end

  private

  def task_params
    params.require(:problem).permit(:capacity, weights: [], values: [])
  end
end
