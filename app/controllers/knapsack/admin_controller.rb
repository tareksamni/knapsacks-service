class Knapsack::AdminController < ApplicationController
  require 'net/http'
  def shutdown
    # Asynchronously kill Sidekiq workers
    # This will push a kill signal to REDIS
    # Workers(Processes) then reads the signal and self-kill
    Sidekiq::ProcessSet.new.each { |p| p.stop! }

    # Kill Puma cluster
    # Process.kill("TERM", Process.pid)
    uri = URI("http://localhost:9293/stop?token=#{ENV['PUMA_SECRET']}")
    Net::HTTP.get(uri)
  end
end
