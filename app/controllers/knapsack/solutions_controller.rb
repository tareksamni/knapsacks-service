class Knapsack::SolutionsController < ApplicationController
  def show
    @task = Task.find(params[:id])
    return head(:not_found) unless @task.completed?
  end
end
