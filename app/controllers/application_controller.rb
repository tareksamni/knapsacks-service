class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  def handle_errors(object)
    render json: { errors: object.errors.full_messages }, status: :bad_request
  end

  def record_not_found
    head(404)
  end
end
