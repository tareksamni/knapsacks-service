json.task(task.id)
json.status(task.status)
json.timestamps do
  json.submitted(task.created_at.try(:to_i))
  json.started(task.started_at.try(:to_i))
  json.completed(task.completed_at.try(:to_i))
end