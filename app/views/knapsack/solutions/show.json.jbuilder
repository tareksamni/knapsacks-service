json.task(@task.id)
json.problem do
  json.capacity(@task.capacity)
  json.values(@task.values)
  json.weights(@task.weights)
end
json.solution do
  json.items(@task.solution)
  json.time(@task.solution_time)
end