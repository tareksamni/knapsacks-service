json.tasks do
  json.submitted(Task.submitted) do |task|
    json.partial! 'knapsack/tasks/task', task: task
  end
  json.started(Task.started) do |task|
    json.partial! 'knapsack/tasks/task', task: task
  end
  json.completed(Task.completed) do |task|
    json.partial! 'knapsack/tasks/task', task: task
  end
end