class SolveWorker
  include Sidekiq::Worker

  def perform(task_id)
    task = Task.find(task_id)
    task.start
    items = task.values.each_with_index.map do |value, i|
      weight = task.weights[i]
      Knapsack::Item.new(weight, value)
    end
    task.solution = Knapsack::Algorithm.solve(items, task.capacity)
                                       .fetch(:indices, [])
    task.save!
    task.complete
  rescue ActiveRecord::RecordNotFound => e
    Rails.logger.info("SolveWorker: #{e}")
  end
end
