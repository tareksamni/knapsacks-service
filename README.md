# README #

This document provides a homework challenge for candidates for full-stack software engineering positions within Maersk Digital

This project is using:

+ [Ruby][15] 2.4.2
+ [Rails][17] 5.1.4
+ [Sidekiq][16] OSS
+ [PostgreSQL][6] db
+ [redis][7] db
+ bundler gem to install all required gems that are listed in [Gemfile][1].
+ rspec gem as the tests framework. Config can be found in [.rspec][2].
+ [Docker][5] to run the application without any other dependencies using docker engine.

# How to run?

## Using [Docker][3]

[Install Docker Engine][3] on your machine to be able to follow these steps.

run `docker --version` to make sure that docker is installed and running successfully. You should get a message like `Docker version 17.06.2-ce, build cec0b72`. Now you follow these steps:

Also, you will need `docker-compose`. run `docker-compose --version`. You should get a response like `docker-compose version 1.16.1, build 6d1ac21`.

+ Run the application/documentation/redis/postgresql/worker on docker containers
```shell
docker-compose up app worker
```

+ Run tests on a docker container
```shell
docker-compose up app_test
```

+ Run benchmarks on a docker container
```shell
docker-compose up app_benchmark
```

## Using [Play With Docker][19]

All you need to do is to curl/clone the docker-compose.yml file to your playground instance and run the commands just as mentioned in the previous section. For ex.

```shell
$ wget https://username:password@bitbucket.org/tareksamni/knapsacks-service/raw/master/docker-compose.yml
$ docker-compose up app worker
# Now you can visit the API documentation, test the API, check monitor API and web interface, etc.
```

## Without Docker

You need to install the dependencies first, follow these steps to install all required dependencies using [`RVM`][4] + You will also need [Postgresql][6] and [redis][7] databases:

+ Install [Postgresql][6] on Mac machine
```shell
$ brew install postgresql
```

+ Install [Redis][7] on Mac machine
```shell
$ brew install redis
```

+ Install [`RVM`][4]
```shell
$ curl -sSL https://get.rvm.io | bash -s stable
```

+ Install/Use ruby 2.4.1 and create a gemset for this project
```shell
$ rvm use ruby-2.4.1@knapsack-service --create
```

+ Install bundler gem
```shell
$ gem install bundler
```

+ Install gems listed in [Gemfile][1]
```shell
$ bundle install
```

+ Run application
```shell
$ ./docker/run.sh
```

+ Run [tests/specs][5]
```shell
$ bundle exec rspec
```

+ Run benchmarking task
```shell
$ bundle exec rake benchmark:run
```

# System architecture

![System architecture](https://bytebucket.org/tareksamni/knapsacks-service/raw/8f5f77c54f0d7513c401ba9853f86bff528a22fc/documentation/Architecture.png?token=209374e135a2150da0f2207d8838f7bedaf9102c)

# API documentation

[Swagger][8] as a tool is used in this project for API documentation together with [Swagger UI][9] to provide a rich web documentation and also the ability to try the APIs from Swagger interface with basic examples.

The API documentation is basically extracted from the implemented [integration tests][10]. Every time you change some APIs and change the tests to match the new requirements, your API documentation will be automatically updated.

You need to run the web application to be able to access the API documentation:

+ Run the web application
```shell
docker-compose up app
```

+ Visit API doc.
```shell
http://localhost:3000/api-docs/
```
![Swagger](https://bytebucket.org/tareksamni/knapsacks-service/raw/019a70a90f2f228732325a3e4cb60a54866891df/documentation/Swagger.png?token=eeb036a4af2b1e9beef97a6ed5b00e36c1e635b3)

# Tests/Specs

[Rspec][11] is used in this project for writing specs and tests. Basically, there are 3 categories of tests included in the project:

## [Integration testing][10]

These tests are grouped in single file per controller and aims to test the functionality from external/3rd part point of view by directly hitting each endpoint with different scenarios and expect a certain response content and codes.

These tests are also used to generate [Swagger][8] API documentation mentioned earlier.

## [Model unit testing][12]

The goal of this kind of tests is to unit test:

1. Data model validations.

2. Model states with different methods calls.

3. Each method behavior.

## [Knapsack unit testing][13]

This part of the project (Algorithm ruby implementation) was imported from [here][14]. So the goal behind this test was to test:

1. Basic data container `Knapsack::Item`.

2. Edge cases behavior like: `empty input` & `input with resulted empty solution`.

3. Normal scenario mentioned by the implementer of the algorithm.

4. Different normal expected scenarios.

5. Different type of invalid inputs. like: `capacity is not Integer`, `items is not Array` & `items is not Array of Knapsack::Algorithm`.

# Background Async Jobs/Workers monitoring

[Sidekiq][16] is used in this project backed by [redis][7] in-memory database to run background async workers. Mainly, this is used to solve the submitted knapsack problems asynchronously without blocking the http response (process) for long time.

Active running, failed, scheduled and dead jobs/workers can be monitored through a nice interface from the following link together with some stats about `redis` consumption:
```shell
http://localhost:3000/monitor/
```
![Sidekiq](https://bytebucket.org/tareksamni/knapsacks-service/raw/9b76752c1a38bcabf476a8d5f0ec197e36412a25/documentation/Sidekiq.png?token=65d299973883afbfc06ff43576b1ce88a57dd83a)
![Redis consumption](https://bytebucket.org/tareksamni/knapsacks-service/raw/9b76752c1a38bcabf476a8d5f0ec197e36412a25/documentation/Redis.png?token=fc4615b4d3469c2858b1e628a466f3017f1f735b)

Also, an API access to these data (in JSON format) could be found here:
```shell
http://localhost:3000/monitor/stats
```
Remember, The web application is always needed to be running to access the monitor interface and API.

# Benchmarking Knapsack Algorithm implementation

A [benchmark rake task][18] is implemented to benchmark the performance of the Knapsack algorithm implementation with 2 variables in the problem:

1. Capacity.

2. Items count.

This task will run the algorithm against combination of `10, 100, 1_000, 10_000, 100_000` items with `10, 100, 1_000` different capacities. It will project the total time used to run each combination and also the details user/system time. An example run is:

```shell
app_benchmark_1  |                                user      system      total        real
app_benchmark_1  | 10 items, 10 capacity        0.010000   0.000000   0.010000  (  0.021548)
app_benchmark_1  | 10 items, 100 capacity       0.000000   0.000000   0.000000  (  0.000264)
app_benchmark_1  | 10 items, 1000 capacity      0.000000   0.000000   0.000000  (  0.002190)
app_benchmark_1  | 100 items, 10 capacity       0.000000   0.000000   0.000000  (  0.000659)
app_benchmark_1  | 100 items, 100 capacity      0.000000   0.000000   0.000000  (  0.002418)
app_benchmark_1  | 100 items, 1000 capacity     0.020000   0.000000   0.020000  (  0.016675)
app_benchmark_1  | 1000 items, 10 capacity      0.010000   0.000000   0.010000  (  0.014372)
app_benchmark_1  | 1000 items, 100 capacity     0.030000   0.000000   0.030000  (  0.033653)
app_benchmark_1  | 1000 items, 1000 capacity    0.190000   0.000000   0.190000  (  0.186029)
app_benchmark_1  | 10000 items, 10 capacity     0.050000   0.000000   0.050000  (  0.051599)
app_benchmark_1  | 10000 items, 100 capacity    0.220000   0.000000   0.220000  (  0.223489)
app_benchmark_1  | 10000 items, 1000 capacity   1.440000   0.020000   1.460000  (  1.460566)
app_benchmark_1  | 100000 items, 10 capacity    0.440000   0.000000   0.440000  (  0.441415)
app_benchmark_1  | 100000 items, 100 capacity   1.670000   0.010000   1.680000  (  1.682274)
app_benchmark_1  | 100000 items, 1000 capacity  16.060000  0.080000   16.140000 ( 16.156268)
```

The benchmark task can be run with:

```shell
docker-compose up app_benchmark
```

# Shutdown API & Background workers

 The web application and background workers can be easily terminated/stopped using the HTTP API as follows:

 ```
 $ curl -XPOST http://localhost:3000/knapsack/admin/shutdown
 ```

 This call will asynchronously send a TERM signal to Sidekiq (background) workers through redis DB and then it will send a stop (TERM signal) to Puma control server.

# Future work

This application delivers the basic requirements and demo my technical abilities but to provide a production ready scalable and stable web application there are still a lot of staff missing such as:

1. Scaling mechanism/policy. the solution architecture is designed to easily scale horizontally by adding more web and worker nodes but the scaling policy and mechanism is still missing. In a real production environment, I'd use Docker Swarm or Kubernetes to define scalability policy.

2. redis cluster. Even though I use a single redis instance without any customer config. But in a production environment I'd use a scalable redis cluster like AWS ElasticCache or Redis cloud (by RedisLabs).

3. As mentioned in #1, to handle elastic scalability we will need a load balancer in front of the web apps with a static defined IP. This load balancer should be able to discover new nodes and also detect dead nodes to remove them from the up stream. this could be achieved whether using the docker cluster or a service discovery tool like Kong.

4. For security purposes, I'd firewall all of the servers/apps against the public internet and only expose the load balancer as a single entry point to the service. And if the service is meant to be an internal microservice, I wouldn't even choose to expose it to the public internet.

5. Ruby has a GIL that prevents it from doing a real multi-threading. So, scaling up the ruby app on the same machine will increasingly require creating new processes or process forks out of the parent process. this will consume way more RAM memory than other languages that can do multi-threading specially with a language like GoLang with its lightweight Go Routines. But, the implemented ruby on rails app was way easier and faster to implement using the community open source projects and 3rd part libs.

6. Missing a web server proxy like Nginx. This solution is directly accessing the web server (Puma) without any proxy layer and I'd use Nginx to deliver a set of powerful features like serving static assets, caching, API rate limiting, etc.

In real life scenarios, I would compare time cost, delivered value/benefits, project requirements to decide which programming language, framework and tools to use. I've decided to use a language/framework that I'm really experienced with to demo my abilities of using 3rd party open sources libs and solutions to deliver the requirements as soon as possible with minimum time invested.


[1]: https://bitbucket.org/tareksamni/knapsacks-service/raw/master/Gemfile
[2]: https://bitbucket.org/tareksamni/knapsacks-service/raw/master/.rspec
[3]: https://docs.docker.com/engine/installation/
[4]: https://rvm.io/
[5]: https://bitbucket.org/tareksamni/knapsacks-service/raw/master/spec
[6]: https://www.postgresql.org/
[7]: https://redis.io/
[8]: https://swagger.io
[9]: https://swagger.io/swagger-ui/
[10]: https://bitbucket.org/tareksamni/knapsacks-service/src/master/spec/integration/
[11]: http://rspec.info/
[12]: https://bitbucket.org/tareksamni/knapsacks-service/src/master/spec/models/
[13]: https://bitbucket.org/tareksamni/knapsacks-service/src/master/spec/lib/knapsack
[14]: https://rosettacode.org/wiki/Knapsack_problem/0-1#Ruby
[15]: https://www.ruby-lang.org/en/
[16]: http://sidekiq.org/
[17]: http://rubyonrails.org/
[18]: https://bitbucket.org/tareksamni/knapsacks-service/src/master/lib/tasks/benchmark.rake
[19]: https://play-with-docker.com