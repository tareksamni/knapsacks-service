require 'benchmark'
namespace :benchmark do
  desc 'Run a benchmark for Knapsack Algorithm'
  task run: :environment do
    Benchmark.bm do |x|
      [10, 100, 1_000, 10_000, 100_000].each do |items_count|
        [10, 100, 1_000].each do |capacity|
          x.report("#{items_count} items, #{capacity} capacity") do
            Knapsack::Algorithm.solve(items(items_count, capacity), capacity)
          end
        end
      end
    end
  end

  def items(i, max)
    i.times.map do
      Knapsack::Item.new(
        Faker::Number.between(1, max),
        Faker::Number.between(1, max)
      )
    end
  end
end