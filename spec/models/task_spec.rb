require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:task) { { capacity: capacity, values: values, weights: weights } }
  let(:capacity) { 100 }
  let(:values) { [1, 5, 10] }
  let(:weights) { [90, 10, 100] }

  it 'is valid with valid attributes' do
    expect(Task.new(task)).to be_valid
    expect(Task.new({})).to be_invalid
  end

  it 'is not valid with a status other than completed, started or submitted' do
    expect(Task.new(task.merge(status: :bla_bla))).to be_invalid
    expect(Task.new(task.merge(status: Task::STATUS_COMPLETED))).to be_valid
    expect(Task.new(task.merge(status: Task::STATUS_STARTED))).to be_valid
    expect(Task.new(task.merge(status: Task::STATUS_SUBMITTED))).to be_valid
  end

  it 'is not valid without a capacity' do
    task.delete(:capacity)
    expect(Task.new(task)).to be_invalid
  end

  it 'is not valid without values' do
    task.delete(:values)
    expect(Task.new(task)).to be_invalid
  end

  it 'is not valid without weights' do
    task.delete(:weights)
    expect(Task.new(task)).to be_invalid
  end

  it 'changes status from submitted to started with start event' do
    new_task = Task.create(task)
    expect(new_task.status).to eq(Task::STATUS_SUBMITTED)
    new_task.start
    expect(new_task.status).to eq(Task::STATUS_STARTED)
  end

  it 'changes status from started to completed with complete event with a solution' do
    new_task = Task.create(task.merge(status: Task::STATUS_STARTED, solution: [1]))
    expect(new_task.status).to eq(Task::STATUS_STARTED)
    new_task.complete
    expect(new_task.status).to eq(Task::STATUS_COMPLETED)
  end

  it 'changes status from started to completed with an empty solution' do
    new_task = Task.create(task.merge(status: Task::STATUS_STARTED))
    expect(new_task.status).to eq(Task::STATUS_STARTED)
    new_task.complete
    expect(new_task.status).to eq(Task::STATUS_COMPLETED)
  end

  it 'sets started_at with start event' do
    new_task = Task.create(task)
    expect(new_task.started_at).to be_nil
    new_task.start
    expect(new_task.started_at).to_not be_nil
  end

  it 'sets completed_at with complete event' do
    new_task = Task.create(task.merge(status: Task::STATUS_STARTED, solution: [1]))
    expect(new_task.completed_at).to be_nil
    new_task.complete
    expect(new_task.completed_at).to_not be_nil
  end

  it 'provides solution_time when status is completed' do
    new_task = Task.create(status: Task::STATUS_COMPLETED)
    new_task.started_at = Time.now - 100.seconds
    new_task.completed_at = Time.now
    expect(new_task.solution_time).to eq(100)
  end

  it 'provides solution_time as 0 when status is not completed' do
    new_task = Task.create
    new_task.started_at = Time.now - 100.seconds
    new_task.completed_at = Time.now
    expect(new_task.solution_time).to eq(0)
  end

  it 'sets status to submitted by default' do
    new_task = Task.create
    expect(new_task.status).to eq(Task::STATUS_SUBMITTED)
  end

  it 'sets solution to empty array by default' do
    new_task = Task.create
    expect(new_task.solution).to eq([])
  end
end