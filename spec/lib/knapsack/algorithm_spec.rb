require 'spec_helper'

describe Knapsack::Algorithm do
  let(:items) {
    [
      Knapsack::Item.new(9, 150),   Knapsack::Item.new(13, 35),
      Knapsack::Item.new(153, 200), Knapsack::Item.new(50, 160),
      Knapsack::Item.new(15, 60),   Knapsack::Item.new(68, 45),
      Knapsack::Item.new(27, 60),   Knapsack::Item.new(39, 40),
      Knapsack::Item.new(23, 30),   Knapsack::Item.new(52, 10),
      Knapsack::Item.new(11, 70),   Knapsack::Item.new(32, 30),
      Knapsack::Item.new(24, 15),   Knapsack::Item.new(48, 10),
      Knapsack::Item.new(73, 40),   Knapsack::Item.new(42, 70),
      Knapsack::Item.new(43, 75),   Knapsack::Item.new(22, 80),
      Knapsack::Item.new(7, 20),    Knapsack::Item.new(18, 12),
      Knapsack::Item.new(4, 50),    Knapsack::Item.new(30, 10)
    ]
  }

  it 'returns empty solution if got empty input' do
    solution = Knapsack::Algorithm.solve([], 100)
    expect(solution[:indices]).to be_an(Array)
    expect(solution[:indices]).to eq([])
  end

  it 'returns empty solution if no solution found' do
    solution = Knapsack::Algorithm.solve([Knapsack::Item.new(150, 10)], 100)
    expect(solution[:indices]).to be_an(Array)
    expect(solution[:indices]).to eq([])
  end

  it 'solves Knapsack problems' do
    solution = Knapsack::Algorithm.solve(items, 400)
    expect(solution[:indices]).to eq([0, 1, 2, 3, 4, 6, 10, 15, 16, 17, 18, 20])
    expect(solution[:value]).to eq(396)
    expect(solution[:cost]).to eq(1030)

    solution = Knapsack::Algorithm.solve(items, 10)
    expect(solution[:indices]).to eq([0])
    expect(solution[:value]).to eq(9)
    expect(solution[:cost]).to eq(150)

    item = Knapsack::Item.new(10, 50)
    solution = Knapsack::Algorithm.solve([item], 10)
    expect(solution[:indices]).to eq([0])
    expect(solution[:value]).to eq(10)
    expect(solution[:cost]).to eq(50)

    item = Knapsack::Item.new(10, 50)
    solution = Knapsack::Algorithm.solve([item], 0)
    expect(solution[:indices]).to eq([])
    expect(solution[:value]).to eq(0)
    expect(solution[:cost]).to eq(0)
  end

  it 'raises an ArgumentError if capacity is not Integer' do
    expect { Knapsack::Algorithm.solve(items, '') }.to raise_error(ArgumentError)
  end

  it 'raises an ArgumentError if items is not Array' do
    expect { Knapsack::Algorithm.solve(1, 400) }.to raise_error(ArgumentError)
  end

  it 'raises an ArgumentError if items is not Array of Knapsack::Algorithm' do
    expect { Knapsack::Algorithm.solve([1], 400) }.to raise_error(ArgumentError)
  end
end