require 'spec_helper'

describe Knapsack::Item do
  it 'sets/gets weight and value' do
    item = Knapsack::Item.new(100, 10)
    expect(item.weight).to eq(100)
    expect(item.value).to eq(10)
  end
end