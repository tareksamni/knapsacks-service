require 'swagger_helper'

describe 'Admin Tasks API' do
  items_count = Faker::Number.between(10, 30)
  problem = {
    capacity: Faker::Number.between(0, 500),
    weights: items_count.times.map { Faker::Number.between(1, 200) },
    values: items_count.times.map { Faker::Number.between(1, 200) },
    status: :submitted
  }
  path '/knapsack/admin/tasks' do
    get 'List of submitted, running, and completed tasks' do
      tags 'Admin Tasks'
      consumes 'application/json'

      response '200', 'List of tasks' do
        before do |example|
          5.times do
            Task.create!(problem)
          end
          submit_request(example.metadata)
        end

        it 'returns submitted/running/completed tasks' do |example|
          assert_response_matches_metadata(example.metadata)
          data = JSON.parse(response.body)
          expect(data['tasks']).to be_a(Hash)
          submitted = data.dig('tasks', 'submitted')
          started = data.dig('tasks', 'started')
          completed = data.dig('tasks', 'completed')

          expect(data['tasks']).to be_a(Hash)
          expect(submitted).to be_a(Array)
          expect(started).to be_a(Array)
          expect(completed).to be_a(Array)
          expect(submitted.size).to eq(5)
          expect(started.size).to eq(0)
          expect(completed.size).to eq(0)

          task = submitted.first
          expect(task['task']).to eq(1)
          expect(task['status']).to eq('submitted')
          timestamps = task['timestamps']
          expect(timestamps['submitted']).to_not be_nil
          expect(timestamps['started']).to be_nil
          expect(timestamps['completed']).to be_nil
        end
      end
    end
  end
end
