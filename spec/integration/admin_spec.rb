require 'swagger_helper'
require 'net/http'

describe 'Admin API' do
  path '/knapsack/admin/shutdown' do
    post 'Shutdowns Puma (web server) and SideKick (background worker)' do
      tags 'Admin'
      consumes 'application/json'
      response '204', 'System shutdowned' do
        before do |example|
          allow_any_instance_of(Sidekiq::Process).to receive(:stop!).and_return(true)
          # allow(Process).to receive(:kill).and_return(true)
          allow(Net::HTTP).to receive(:get).and_return(true)
          submit_request(example.metadata)
        end

        it 'returns a valid 204 response' do |example|
          assert_response_matches_metadata(example.metadata)
        end
      end
    end
  end
end