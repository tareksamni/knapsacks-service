require 'swagger_helper'

describe 'Solutions API' do
  items_count = Faker::Number.between(10, 30)
  problem = {
    capacity: Faker::Number.between(0, 500),
    weights: items_count.times.map { Faker::Number.between(1, 200) },
    values: items_count.times.map { Faker::Number.between(1, 200) }
  }
  path '/knapsack/solutions/{id}' do
    get 'Retrieves a solution' do
      tags 'Solutions'
      produces 'application/json'
      parameter name: :id, in: :path, type: :string

      response '200', 'solution found' do
        schema type: :object,
               properties: {
                 task: { type: :integer },
                 problem: {
                   type: :object,
                   properties: {
                     capacity: { type: :integer },
                     weights: {
                       type: 'array',
                       items: { type: :integer }
                     },
                     values: {
                       type: 'array',
                       items: { type: :integer }
                     }
                   }
                 },
                 solution: {
                   type: :object,
                   properties: {
                     items: {
                       type: 'array',
                       items: { type: :integer }
                     },
                     time: { type: :integer }
                   }
                 }
               },
               required: [ 'task', 'problem', 'solution' ]
        let(:id) { Task.create!(problem.merge(completed_at: Time.now, status: :completed)).id }
        run_test! do |response|
          data = JSON.parse(response.body)
          expect(data['task']).to eq(id)

          problem_data = data['problem']
          expect(problem_data['values']).to be_an(Array)
          expect(problem_data['values'].first).to be_an(Integer)
          expect(problem_data['weights']).to be_an(Array)
          expect(problem_data['weights'].first).to be_an(Integer)
          expect(problem_data['capacity']).to be_an(Integer)

          solution_data = data['solution']
          expect(solution_data['items']).to be_an(Array)
          expect(solution_data['time']).to be_an(Integer)
        end
      end

      response '404', 'solution not found' do
        before do |example|
          submit_request(example.metadata)
        end

        let(:id) { 'invalid' }
        it 'returns 404 if task not found' do |example|
          assert_response_matches_metadata(example.metadata)
        end

        let(:id) { Task.create!(problem).id }
        it 'task found but without a solution' do |example|
          assert_response_matches_metadata(example.metadata)
        end
      end
    end
  end
end