require 'swagger_helper'

describe 'Tasks API' do
  items_count = Faker::Number.between(10, 30)
  problem = {
    capacity: Faker::Number.between(0, 500),
    weights: items_count.times.map { Faker::Number.between(1, 200) },
    values: items_count.times.map { Faker::Number.between(1, 200) }
  }
  path '/knapsack/tasks' do
    post 'Creates a task' do
      tags 'Tasks'
      consumes 'application/json'
      parameter name: :task, in: :body, schema: {
        type: :object,
        properties: {
          problem: {
            type: :object,
            properties: {
              capacity: { type: :integer },
              weights: {
                type: :array,
                items: { type: :integer }
              },
              values: {
                type: :array,
                items: { type: :integer }
              }
            }
          }
        },
        required: [ 'capacity', 'weights', 'values' ]
      }

      response '200', 'task created' do
        schema type: :object,
               properties: {
                 task: { type: :integer },
                 status: { type: :string },
                 timestamps: {
                   type: :object,
                   properties: {
                     submitted: { type: :datetime },
                     started: { type: :datetime },
                     completed: { type: :datetime }
                   }
                 }
               },
               required: [ 'task', 'status', 'timestamps' ]
        let(:task) { { problem: problem } }
        run_test! do |response|
          data = JSON.parse(response.body)
          expect(data['task']).to be_a(Integer)
          expect(data['status']).to eq('submitted')
          expect(data['timestamps']).to be_a(Hash)
          expect(data.dig('timestamps', 'submitted')).to be_a(Integer)
          expect(data.dig('timestamps', 'started')).to be_nil
          expect(data.dig('timestamps', 'completed')).to be_nil
        end
      end

      response '400', 'invalid request (problem is missing)' do
        let(:task) { {  } }
        it 'raise an error ActionController::ParameterMissing' do |example|
          expect{ submit_request(example.metadata) }.to raise_error(ActionController::ParameterMissing)
        end
      end

      response '400', 'invalid request (problem is invalid)' do
        schema type: :object,
               properties: {
                 errors: {
                   type: :array,
                   items: { type: :string }
                 }
               }

        let(:task) { { problem: { capacity: 100 } } }
        run_test! do |response|
          data = JSON.parse(response.body)
          expect(data['errors']).to be_an(Array)
          expect(data['errors'].size).to eq(2)
        end
      end
    end
  end

  path '/knapsack/tasks/{id}' do
    get 'Retrieves a task' do
      tags 'Tasks'
      produces 'application/json'
      parameter name: :id, in: :path, type: :string

      response '200', 'task found' do
        schema type: :object,
               properties: {
                 task: { type: :integer },
                 status: { type: :string },
                 timestamps: {
                   type: :object,
                   properties: {
                     submitted: { type: :datetime },
                     started: { type: :datetime },
                     completed: { type: :datetime }
                   }
                 }
               },
               required: [ 'task', 'status', 'timestamps' ]
        let(:id) { Task.create({ status: :submitted }.merge(problem)).id }
        run_test! do |response|
          data = JSON.parse(response.body)
          expect(data['task']).to eq(id)
          expect(data['status']).to eq('submitted')
          expect(data['timestamps']).to be_a(Hash)
          expect(data.dig('timestamps', 'submitted')).to be_a(Integer)
          expect(data.dig('timestamps', 'started')).to be_nil
          expect(data.dig('timestamps', 'completed')).to be_nil
        end
      end

      response '404', 'task not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end