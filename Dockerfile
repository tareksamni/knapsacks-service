FROM ruby:2.4.2

# Set APP_HOME and WORKDIR
ENV APP_HOME /app
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

# Install Gems
ADD Gemfile* $APP_HOME/
ADD docker $APP_HOME/docker/
RUN bash docker/bundle.sh

# Add the Rails app
ADD . $APP_HOME

# Create user and group
RUN groupadd --gid 9999 app && \
    useradd --uid 9999 --gid app app && \
    chown -R app:app $APP_HOME

# Precompile assets
RUN RAILS_ENV=production bundle exec rake assets:precompile

# Save timestamp of image building
RUN date -u > BUILD_TIME