#! /bin/bash
bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:setup
bundle exec puma --control tcp://0.0.0.0:9293 --control-token $PUMA_SECRET